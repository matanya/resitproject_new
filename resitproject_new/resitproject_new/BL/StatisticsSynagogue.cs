﻿using resitproject_new.DAL;
using resitproject_new.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace resitproject_new.BL
{
    public class StatisticsSynagogue
    {
        private int _synagogueId;
        private List<Chair> _chairs;
        private List<Absence> _absences;

        public StatisticsSynagogue(int synagogueId)
        {
            _synagogueId = synagogueId;
        }

        public StatisticsViewModel GetStatisticsByUsers()
        {
            ResetModel();

            var absenceViewModel = new List<AbsenceViewModel>();

            foreach (var item in _absences)
            {
                absenceViewModel.Add(AbsenceViewModel.FromAbsence(item));
            }

            var absencesByUser = absenceViewModel.GroupBy(a => a.User).
                Select(g => new {
                    g.Key,
                    Values = g.GroupBy(a => a.Date.Substring(0, 2)).
                Select(u => new { u.Key, Count = u.Count() })
                }).ToList();

            var statisticsUser = new StatisticsViewModel()
            {
                HebrewTitle = "מתפללים - ממוצע חיסורים בחודש",
                Title = "users",
                Keys = absencesByUser.Select(a => a.Key.UserName).ToArray(),
                Values = absencesByUser.Select(a => (int)(a.Values.Select(m => m.Count).Average())).ToArray()
            };

            return statisticsUser;

        }

        public StatisticsViewModel GetStatisticsByDates()
        {

            ResetModel();
            var absencesByDate = _absences.GroupBy(a => a.Date).Select(g => new { g.Key, Values = g.Count() }).ToList();

            var statisticsDate = new StatisticsViewModel()
            {
                HebrewTitle = "אחוזי תפוסה בשבתות",
                Title = "dates",
                Keys = absencesByDate.Select(a => a.Key.ToShortDateString()).ToArray(),
                Values = absencesByDate.Select(a => 100 - (100 / _chairs.Count() * a.Values)).ToArray()
            };

            return statisticsDate;

        }


        private void ResetModel()
        {
            var lastYear = DateTime.Now.AddYears(-1);
            _chairs = RepositoryQueries.GetAllChairsInSynagogue(_synagogueId).ToList();
            _absences = RepositoryQueries.GetAbsencesBySynagogue(_synagogueId).
                Where(a => DbFunctions.TruncateTime(a.Date) > DbFunctions.TruncateTime(lastYear)).ToList();
        }
    }
}