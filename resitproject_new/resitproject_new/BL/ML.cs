﻿using Microsoft.Data.DataView;
using Microsoft.ML;
using Microsoft.ML.Data;
using resitproject_new.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace resitproject_new.BL
{
    public class ML
    {
         
        public static string Prdeict()
        {
            return "";
        }

        public static string Train(DateTime date, string name)
        {
            
            MLContext mlContext = new MLContext();

            
            IDataView trainingDataView = mlContext.Data.ReadFromEnumerable(AbsenceData.GetAbsencesData());
           
           var pipeline = mlContext.Transforms.Conversion.MapValueToKey("Label")
                .Append(mlContext.Transforms.Concatenate("Features", "Name", "Date"))
                .AppendCacheCheckpoint(mlContext)
                .Append(mlContext.MulticlassClassification.Trainers.StochasticDualCoordinateAscent(labelColumn: "Label", featureColumn: "Features"))
                .Append(mlContext.Transforms.Conversion.MapKeyToValue("AbsenceLabels"));

             
            var model = pipeline.Fit(trainingDataView);

           
            var prediction = model.CreatePredictionEngine<AbsenceData, AbsencePrediction>(mlContext).Predict(
                new AbsenceData()
                {
                    Date = date.ToShortDateString(),
                    Name= name
                });

            return prediction.AbsenceLabels;

        }
    }

    public class AbsencePrediction
    {

        public string AbsenceLabels;
    }

    public class AbsenceData
    {
        public string Name;
        public string Date;
        public string Label;

        public static IEnumerable<AbsenceData> GetAbsencesData()
        {
            List<AbsenceData> absenceData = new List<AbsenceData>();
            var absences = RepositoryQueries.GetAbsences().ToList();
            var users = RepositoryQueries.GetUsers().Where(u => u != null).ToList();
            foreach (var absence in absences)
            {
                foreach (var chair in absence.Chairs)
                {
                    absenceData.Add(new AbsenceData() { Date = absence.Date.ToShortDateString(), Name = chair.User.UserName, Label = "true"});
                }
                
            }

            foreach (var absence in absences)
            {
                foreach (var user in users)
                {
                    var test = absenceData.FirstOrDefault(a => a.Name == user.UserName && a.Date == absence.Date.ToShortDateString() && a.Label == "true");
                    if (test == null)
                    {
                        absenceData.Add(new AbsenceData() { Date = absence.Date.ToShortDateString(), Name = user.UserName, Label = "false" });
                    }
                }
            }

           return absenceData;
        }
    }

}