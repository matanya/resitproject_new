﻿using resitproject_new.DAL;
using resitproject_new.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;

namespace resitproject_new.BL
{
    public class CSVReader
    {
        private static ApplicationUserManager _userManager;
        private static CSVReader instance;


        private CSVReader()
        {

        }


        public static CSVReader Create(ApplicationUserManager userManager)
        {
            if (_userManager == null)
            {
                _userManager = userManager;
                instance = new CSVReader();
            }


            return instance;
        }

        
        public void ReadChairCsv(string path, Synagogue synagogue)
        {
           
            
            List<Chair> chairs = new List<Chair>();
            var lines = File.ReadAllLines(path);
            for (var lineIndex = 0; lineIndex < lines.Length; lineIndex++)
            {
                var cloumns = lines[lineIndex].Split(',').ToList();
                foreach (var col in cloumns)
                {
                    if (col.Length > 0 && col != " ")
                    {
                        chairs.Add(new Chair()
                        {
                            I = cloumns.IndexOf(col),
                            J = lineIndex,
                            Synagogue = synagogue,
                            SerialNumber = int.Parse(col)
                        }
                        );
                    }
                }
            }
            RepositoryQueries.UpdateChairOfSynagogue(synagogue.Id, chairs);

        }

        public async Task  ReadUsersCsv(string path, Synagogue synagogue)
        {


            var lines = File.ReadAllLines(path);
            for (var lineIndex = 1; lineIndex < lines.Length; lineIndex++)

            {
                var cloumns = lines[lineIndex].Split(',').ToList();
                ApplicationUser user = await _userManager.FindByEmailAsync(cloumns[0]);

                if (user == null)
                {
                    user = new ApplicationUser { UserName = cloumns[0], Email = cloumns[0], PhoneNumber = cloumns[1] };
                    await _userManager.CreateAsync(user, "Aa123456!");

                }
            
                var chair = RepositoryQueries.GetChairBySerialNumberOfSynagogueId(int.Parse(cloumns[3]), synagogue.Id);
                RepositoryQueries.UpdateUserChair(chair, user.Id);

            }

        }

    }
}