﻿
class ChairsMapHandler {
    static DEFAULT_CONTAINER_WIDTH = "700px";
    static TEXT_COLOR = "black";
    static DEFAULT_CHAIR_COLOR = "white";
    _container;
    _canvas;
    _ctx;
    _chairSize;
    _allChairs;
    _chairsToMark;
    _markColor;


    createChairsMap(containerId, allChairs, chairsToMark, markColor) {
        this._allChairs = allChairs;
        this._chairsToMark = chairsToMark;
        this._markColor = markColor;

        this._container = document.getElementById(containerId);
        this._container.style.width = this._container.style.width || ChairsMapHandler.DEFAULT_CONTAINER_WIDTH;
        this._canvas = document.createElement("canvas");
        this._container.appendChild(this._canvas);
        this._canvas.width = this._container.clientWidth;

        const iArray = allChairs.map(c => c.I);
        const jArray = allChairs.map(c => c.J);
        const colAmount = Math.max(...iArray) + 1;
        const rowAmount = Math.max(...jArray) + 1;

        this._chairSize = this._canvas.width / colAmount;
        this._canvas.height = rowAmount * this._chairSize;

        this._ctx = this._canvas.getContext("2d");

        this.initMap();

        this._canvas.addEventListener('click', (e) => {
            const mousePos = {
                x: e.offsetX,
                y: e.offsetY
            };

            const i = parseInt(mousePos.x / this._chairSize);
            const j = parseInt(mousePos.y / this._chairSize);

            const clickedChair = allChairs.find(c => c.I == i && c.J == j);
            if (clickedChair) {
                const event = new CustomEvent('chairClicked', { detail: { clickedChair } });
                document.dispatchEvent(event);
            }
        });
    }

    initMap() {
        for (var chair of this._allChairs) {
            const fillColor = (this._chairsToMark && this._chairsToMark.some(c => c.SerialNumber == chair.SerialNumber)) ? this._markColor : ChairsMapHandler.DEFAULT_CHAIR_COLOR;
            this.markChair(chair, fillColor);
        }
    }

    markChair(chair, color) {
        const userName = chair.User ? chair.User.UserName : "";
        const chairNumber = chair.SerialNumber;
        const x = chair.I * this._chairSize;
        const y = chair.J * this._chairSize;
        this._ctx.fillStyle = color;
        this._ctx.fillRect(x, y, this._chairSize, this._chairSize);
        this._ctx.strokeRect(x, y, this._chairSize, this._chairSize);

        this._ctx.fillStyle = ChairsMapHandler.TEXT_COLOR;
        this._ctx.fillText(chairNumber, x + this._chairSize / 2, y + this._chairSize / 3);
        this._ctx.fillText(userName, x, y + this._chairSize / 3 * 2, this._chairSize);
    }
}
