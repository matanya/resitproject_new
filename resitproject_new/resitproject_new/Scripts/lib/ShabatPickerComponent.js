﻿class ShabatPickerComponent {
    _dateComponentInnerHtml = `
            <div id="ShabatPickerComponent">
                <button id="date-left-button" type="button">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </button>                
                <div>
                    <p id="hebrewDate"></p>
                    <p id="hebrewTitle"></p>
                </div>
                <button id="date-right-button" type="button">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </button>
            </div>
        `
    _userDate;
    _shabatProvider;
    currentShabat;
    _enablePast;


    constructor(elementId, initialDate, enablePast) {
        this._enablePast = enablePast || false;
        $(`#${elementId}`).append(this._dateComponentInnerHtml);

        this._userDate = initialDate ? new Date(initialDate) : new Date();
        this._shabatProvider = new ShabatsProvider((shabat) => this.writeDate(shabat));
        this._shabatProvider.getShabatsByDate(this._userDate);

        $("#date-left-button").click(() => this.changeDate(1));
        $("#date-right-button").click(() => this.changeDate(-1));
    }

    changeDate(factor) {
        var temp = new Date(this._userDate.getFullYear(), this._userDate.getMonth(), this._userDate.getDate() + 7 * factor);
        this._userDate = temp;
        this._shabatProvider.getShabatsByDate(this._userDate);
    }

    writeDate(shabat) {
        this.currentShabat = shabat;
        if (shabat) {         
            var event = new CustomEvent('shabatChanged', { detail: { date: shabat.date } });
            document.dispatchEvent(event);
            $("#hebrewTitle").text(shabat.hebrewTitle);
            $("#hebrewDate").text(shabat.hebrewDate);


            if (!this._enablePast) {
                var currentShabatDate = new Date(shabat.date);
                var previousShabatDate = new Date(currentShabatDate.getFullYear(), currentShabatDate.getMonth(), currentShabatDate.getDate() - 7);
                if (previousShabatDate < new Date()) {
                    $("#date-right-button").attr("disabled", true);
                }
                else {
                    $("#date-right-button").attr("disabled", false);
                }
            }
            
        }
    }
}