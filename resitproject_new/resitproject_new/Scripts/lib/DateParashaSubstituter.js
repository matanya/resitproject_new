﻿class DateParashaSubstituter {
    static SubstituteDateToParashaForSelector(selector) {
        $(selector).each((index, item) => {
            const shabatProvider = new ShabatsProvider((result) => {
                $(item).text(result.hebrewTitle);
            });
            const date = $(item).text();
            shabatProvider.getShabatsByDate(new Date(date));
        });
    }
}

