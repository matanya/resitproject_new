﻿class AbsenceFormHandler {
    shabatPicker;
    allChairs;
    selectableChairsSerialNumbers;
    selectableChairs;
    selectedChairs;
    selectedChairsSerialNumbers
    date;
    comboBoxElement;
    map;

    shabatPickerElementId = "ShabatPickerContainer";
    selectableChairColor = "green";
    selectedChairColor = "red";
    chairsComboBoxElementSelector = "#Chairs";
    mapContainerId = "mapContainer";
    formContainerSelector = ".form-horizontal";

    constructor(_allChairs, _selectableChairsSerialNumbers, _selectedChairsSerialNumbers, _date) {
        this.allChairs = _allChairs;
        this.selectableChairsSerialNumbers = _selectableChairsSerialNumbers;
        this.selectedChairsSerialNumbers = (_selectedChairsSerialNumbers) ? _selectedChairsSerialNumbers : [];
        this.date = (_date) ? _date : null;

        this.selectableChairs =
            this.intersectChairsWithChairsSerialNumbers(this.allChairs, this.selectableChairsSerialNumbers);
        this.selectedChairs =
            this.intersectChairsWithChairsSerialNumbers(this.allChairs, this.selectedChairsSerialNumbers);

        this.comboBoxElement = $(this.chairsComboBoxElementSelector);
    }

    handleForm() {
        this.initilizeShabatPicker();
        this.errorWhenCantSelectChair();
        this.createMapWithSelectableChairsMarked();
        this.selectInitialSelectedItems();
        this.selectOnMapWhenSelectInCombobox();
        this.selectInComboboxWhenSelectOnMap();
    }

    initilizeShabatPicker() {
        this.shabatPicker = new ShabatPickerComponent(this.shabatPickerElementId, this.date);
    }

    errorWhenCantSelectChair() {
        if (!this.selectableChairs || this.selectableChairs.length === 0) {
            $(formContainerSelector).empty().text("לצערנו אין לך כסא בבית כנסת");
        }
    }

    createMapWithSelectableChairsMarked() {
        this.map = new ChairsMapHandler();
        this.map.createChairsMap(this.mapContainerId, this.allChairs, this.selectableChairs, this.selectableChairColor);
    }

    selectInitialSelectedItems() {
        this.markSelectedChairsOnMap();
        this.selectSelectedChairsOnComboBox(); 
    }

    markSelectedChairsOnMap() {
        this.selectedChairs.forEach(chair => this.map.markChair(chair, this.selectedChairColor));
    }

    selectSelectedChairsOnComboBox() {
        this.comboBoxElement.val(this.selectedChairsSerialNumbers.map(num => num.toString()));
    }

    selectOnMapWhenSelectInCombobox() {
        this.comboBoxElement.change((event) => {
            this.selectedChairsSerialNumbers = $(event.target).val().map(num => parseInt(num));
            this.selectedChairs =
                this.intersectChairsWithChairsSerialNumbers(this.selectableChairs, this.selectedChairsSerialNumbers);

            this.map.initMap();
            this.markSelectedChairsOnMap();
        });
    }

    intersectChairsWithChairsSerialNumbers(chairsArray, chairsSerialNumbersArray) {
        return chairsArray.filter(chair =>
            chairsSerialNumbersArray.some(num => num === chair.SerialNumber));
    }

    selectInComboboxWhenSelectOnMap() {
        document.addEventListener("chairClicked", (e) => {
            const chairClicked = e.detail.clickedChair;

            if (this.isChairSelectable(chairClicked)) {
                const chairClickedNum = chairClicked.SerialNumber;
                //unmark
                if (this.selectedChairsSerialNumbers.some(chairNum => chairClickedNum === chairNum)) {
                    this.selectedChairsSerialNumbers = this.selectedChairsSerialNumbers.filter((chairNum) => chairNum !== chairClickedNum);
                    this.selectedChairs = this.selectedChairs.filter(chair => chairClickedNum !== chair.SerialNumber);
                    this.comboBoxElement.val(this.selectedChairsSerialNumbers.map(num => num.toString()));
                    this.map.markChair(chairClicked, this.selectableChairColor);
                }
                //mark
                else {
                    this.selectedChairsSerialNumbers.push(chairClickedNum);
                    this.selectedChairs.push(chairClicked);
                    this.selectSelectedChairsOnComboBox();
                    this.map.markChair(chairClicked, this.selectedChairColor);
                }
            }
        })
    }

    isChairSelectable(chair) {
        return this.selectableChairs.some(c => c.SerialNumber === chair.SerialNumber);
    }

    
}