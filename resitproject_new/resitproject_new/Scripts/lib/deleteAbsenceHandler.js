﻿class DeleteAbsenceHandler {
    static listenToDeleteClick(selector, urlAfterDelete) {
        $(selector).each((index, element) => {
            $(element).click(() => {
                const isConfirmed = confirm("האם אתה בטוח שברצונך למחוק?");

                if (isConfirmed) {
                    const form = $('#__AjaxAntiForgeryForm');
                    const token = $('input[name="__RequestVerificationToken"]', form).val();
                    const deleteUrl = $(element).attr("href").substring(1);
                    $.ajax({
                        method: "POST",
                        url: deleteUrl,
                        data: {
                            __RequestVerificationToken: token
                        },
                        success: () => {
                            alert("הדיווח נמחק בהצלחה");
                            if (urlAfterDelete) {
                                location.href = urlAfterDelete;
                            }
                        },
                        error: (err) => {
                            if (status.err == 404) {
                                alert("הדיווח שברצונך למחוק לא קיים במאגר הנתונים");
                            }
                            else if (status.err == 403) {
                                alert("אינך רשאי לבצע מחיקה");
                            }
                        }
                    });
                }
            });
        });
    }
}