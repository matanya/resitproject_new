﻿class ShabatsProvider {
    static SERVER_URL = `https://www.hebcal.com/hebcal/`;
    static PARAMETES = `?v=1&cfg=json&maj=on&ss=on&mf=on&c=on&geo=geoname&geonameid=281184&m=50&s=on&D=on`;
    _shabats = [];
    _currnetMonth;
    _currnetYear;
    _callback;
    _date;

    constructor(callback) {
        this._callback = callback;
    }

    createShabatsArray(serverResults) {
        this._shabats = [];
        for (var i = 0; i < serverResults.items.length; i++) {
            if (serverResults.items[i].category === "candles") {
                var item = this.getTitle(serverResults.items, i);
                if (item) {
                    var newSahbat = {
                        hebrewTitle: item.hebrew,
                        hebrewDate: this.getItemByCategory(serverResults.items, i, "hebdate").hebrew,
                        date: serverResults.items[i].date,
                        title: item.title,
                        havdla: this.getItemByCategory(serverResults.items, i, "havdalah").date,
                        candles: serverResults.items[i].date
                    }
                    this._shabats.push(newSahbat);
                } 
            }
        }
        this.getShabatsByDate(this._date);
    }

    getTitle(items, index) {
        for (var i = index; i < items.length; i++) {
            if (items[i].category === "parashat" || items[i].category === "holiday") {
                return items[i];
            }
        }
    }

    getItemByCategory(items, index, category) {
        for (var i = index; i < items.length; i++) {
            if (items[i].category === category) {
                return items[i];
            }
        }
    }

    getShabatsByDate(date) {
        const userMonth = date.getMonth() + 1;
        const userYear = date.getFullYear();
        this._date = date;
        if (userMonth === this._currnetMonth && userYear === this._currnetYear) {
            const shabat = this.getShabatFromList(date);
            if (shabat != undefined) {
                this._callback(this.getShabatFromList(date));
            } 
            else {
                this._currnetMonth = userMonth + 1;
                this._currnetYear = userYear;
                this._date = new Date(this._currnetYear, this._currnetMonth, 1);
                getShabatsFromServer(ShabatsProvider.SERVER_URL + ShabatsProvider.PARAMETES + `&year=${this._currnetYear}&month=${this._currnetMonth}`, this)
            }
        } else {
            this._currnetMonth = userMonth;
            this._currnetYear = userYear;
            getShabatsFromServer(ShabatsProvider.SERVER_URL + ShabatsProvider.PARAMETES + `&year=${this._currnetYear}&month=${this._currnetMonth}`, this)
        }
    }

    getShabatFromList(date) {
        var index = 0;
        while (index < this._shabats.length && new Date(this._shabats[index].date) < date) {
            index++;
        }
        if (index < this._shabats.length) {
            return this._shabats[index];
        } else {
            return undefined;
        }
            
    }
}


function getShabatsFromServer(url, shabatsProvider) {
    $.ajax({
        url: url,
        success: function (result) {
            shabatsProvider.createShabatsArray(result);
        }
    });
}




    

