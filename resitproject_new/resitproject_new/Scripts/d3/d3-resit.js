﻿
var width = 50,
    barHeight = 420;

function createHisogram(id, data) {

    var values = data.Values;
    var x = d3.scale.linear()
        .domain([0, d3.max(values)])
        .range([0, barHeight]);
    var svgId = `#${id}`;
    var charts = d3.select(svgId)
        .attr("width", width * values.length)
        .attr("height", barHeight );

    var bar = charts.selectAll("g")
        .data(values)
        .enter().append("g")
        .attr("transform", function (d, i) { return "translate(" + i * width + "," + (barHeight - x(d)) + ")"; });

    bar.append("rect")
        .attr("width", width - 1)
        .attr("height",  x);

    //keys text
    bar.append("text")
        .attr("x", (function (d) {return width / 2 +  10     +data.Keys[data.Values.indexOf(d)].length;}))
        .attr("y", function (d) { return x(d) - 5 ; } )
        .attr("dy", ".10em")
        .text(function (d, i) { return data.Keys[i];  });

    // values text
    bar.append("text")
        .attr("x", (width / 2))
        .attr("y", 10)
        .attr("dy", ".35em")
        .text(function (d) { return d; });
}







