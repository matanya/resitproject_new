﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(resitproject_new.Startup))]
namespace resitproject_new
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
