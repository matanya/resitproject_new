﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace resitproject_new.Models
{
    public class Absence
    {
        public int Id { get; set; }
        [Display(Name = "תאריך חיסור")]
        public DateTime Date { get; set; }
        public ICollection<Chair> Chairs { get; set; }
    }
}