﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace resitproject_new.Models
{
    public class ChairsUserViewModel
    {
        public ApplicationUser User { get; set; }
        public List<int> Chairs { get; set; }

    }
}