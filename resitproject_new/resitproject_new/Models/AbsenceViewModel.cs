﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace resitproject_new.Models
{
    public class AbsenceViewModel
    {
        public int Id { get; set; }
        [Display(Name = "תאריך חיסור")]
        public string Date { get; set; }
        public string Chairs { get; set; }
        public ApplicationUser User { get; set; }
        public Synagogue Synagogue { get; set; }

        public static AbsenceViewModel FromAbsence(Absence absence)
        {
            return new AbsenceViewModel()
            {
                Id = absence.Id,
                Date = absence.Date.ToString("MM/dd/yyyy"),
                Chairs = string.Join(",", absence.Chairs.Select(chair => chair.SerialNumber).ToArray()),
                Synagogue = absence.Chairs.Select(chair => chair.Synagogue).Distinct().FirstOrDefault(),
                User = absence.Chairs.Select(chair => chair.User).Distinct().FirstOrDefault()
            };
        }
    }
}