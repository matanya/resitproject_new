﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace resitproject_new.Models
{
    public class Chair
    {
        public int Id { get; set; }
        public int I { get; set; }
        public int J { get; set; }
        [Display(Name = "מספר כסא")]
        public int SerialNumber { get; set; }
        public Synagogue Synagogue { get; set; }
        public ApplicationUser User { get; set; }
        public ICollection<Absence> Absences { get; set; }

    }
}