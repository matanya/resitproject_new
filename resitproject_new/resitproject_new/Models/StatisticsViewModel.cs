﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace resitproject_new.Models
{
    public class StatisticsViewModel
    {
        public string Title { get; set; }
        public string HebrewTitle { get; set; }
        public string[] Keys  { get; set; }
        public int[] Values { get; set; }
    }
}