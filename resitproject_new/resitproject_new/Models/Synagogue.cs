﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace resitproject_new.Models
{
    public class Synagogue
    {
        
        [Key]
        public int Id { get; set; }
        [DisplayName("שם בית הכנסת")]
        public String Name { get; set; }
        [DisplayName("כתובת בית הכנסת")]
        public string Address { get; set; }
        public double Lon { get; set; }
        public double Lat { get; set; }
        public ApplicationUser Manager { get; set; }
        public ICollection<Chair> Chairs { get; set; }
        [DisplayName("video")]
        public string VideoPath { get; set; }
    }
}