﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace resitproject_new.Models
{
    public class ChairRequest
    {
        public int Id { get; set; }
        [Required]
        public int SynagogueId { get; set; }
        public Synagogue Synagogue { get; set; }
        [Required]
        public ApplicationUser User { get; set; }
        public DateTime RequestDate { get; set; }
        public bool isTempRequest { get; set; }
        public DateTime ForDate { get; set; }
        public int NumOfChairs { get; set; }
        public int SpecificChairId { get; set; }
        public Chair SpecificChair { get; set; }
    }
}