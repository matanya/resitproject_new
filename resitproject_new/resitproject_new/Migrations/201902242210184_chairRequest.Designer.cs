// <auto-generated />
namespace resitproject_new.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class chairRequest : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(chairRequest));
        
        string IMigrationMetadata.Id
        {
            get { return "201902242210184_chairRequest"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
