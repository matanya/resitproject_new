namespace resitproject_new.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class map : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Synagogues", "lon", c => c.Double(nullable: false));
            AddColumn("dbo.Synagogues", "lat", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Synagogues", "lat");
            DropColumn("dbo.Synagogues", "lon");
        }
    }
}
