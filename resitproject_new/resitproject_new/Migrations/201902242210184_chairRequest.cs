namespace resitproject_new.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class chairRequest : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ChairRequests",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SynagogueId = c.Int(nullable: false),
                        RequestDate = c.DateTime(nullable: false),
                        isTempRequest = c.Boolean(nullable: false),
                        ForDate = c.DateTime(nullable: false),
                        NumOfChairs = c.Int(nullable: false),
                        SpecificChairId = c.Int(nullable: false),
                        User_Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Chairs", t => t.SpecificChairId, cascadeDelete: true)
                .ForeignKey("dbo.Synagogues", t => t.SynagogueId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id, cascadeDelete: true)
                .Index(t => t.SynagogueId)
                .Index(t => t.SpecificChairId)
                .Index(t => t.User_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ChairRequests", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.ChairRequests", "SynagogueId", "dbo.Synagogues");
            DropForeignKey("dbo.ChairRequests", "SpecificChairId", "dbo.Chairs");
            DropIndex("dbo.ChairRequests", new[] { "User_Id" });
            DropIndex("dbo.ChairRequests", new[] { "SpecificChairId" });
            DropIndex("dbo.ChairRequests", new[] { "SynagogueId" });
            DropTable("dbo.ChairRequests");
        }
    }
}
