namespace resitproject_new.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class absencechairs : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Absences", "ChairId", "dbo.Chairs");
            DropIndex("dbo.Absences", new[] { "ChairId" });
            CreateTable(
                "dbo.ChairAbsences",
                c => new
                    {
                        Chair_Id = c.Int(nullable: false),
                        Absence_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Chair_Id, t.Absence_Id })
                .ForeignKey("dbo.Chairs", t => t.Chair_Id, cascadeDelete: true)
                .ForeignKey("dbo.Absences", t => t.Absence_Id, cascadeDelete: true)
                .Index(t => t.Chair_Id)
                .Index(t => t.Absence_Id);
            
            DropColumn("dbo.Absences", "ChairId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Absences", "ChairId", c => c.Int(nullable: false));
            DropForeignKey("dbo.ChairAbsences", "Absence_Id", "dbo.Absences");
            DropForeignKey("dbo.ChairAbsences", "Chair_Id", "dbo.Chairs");
            DropIndex("dbo.ChairAbsences", new[] { "Absence_Id" });
            DropIndex("dbo.ChairAbsences", new[] { "Chair_Id" });
            DropTable("dbo.ChairAbsences");
            CreateIndex("dbo.Absences", "ChairId");
            AddForeignKey("dbo.Absences", "ChairId", "dbo.Chairs", "Id", cascadeDelete: true);
        }
    }
}
