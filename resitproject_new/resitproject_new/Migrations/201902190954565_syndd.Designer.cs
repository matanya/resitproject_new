// <auto-generated />
namespace resitproject_new.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class syndd : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(syndd));
        
        string IMigrationMetadata.Id
        {
            get { return "201902190954565_syndd"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
