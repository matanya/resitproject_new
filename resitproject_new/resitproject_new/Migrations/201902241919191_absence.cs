namespace resitproject_new.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class absence : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Absences",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        ChairId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Chairs", t => t.ChairId, cascadeDelete: true)
                .Index(t => t.ChairId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Absences", "ChairId", "dbo.Chairs");
            DropIndex("dbo.Absences", new[] { "ChairId" });
            DropTable("dbo.Absences");
        }
    }
}
