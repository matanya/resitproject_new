namespace resitproject_new.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addvido : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Synagogues", "VideoPath", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Synagogues", "VideoPath");
        }
    }
}
