namespace resitproject_new.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Changeusertype : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Synagogues", "Manager_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.Synagogues", "Manager_Id");
            AddForeignKey("dbo.Synagogues", "Manager_Id", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Synagogues", "Manager_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Synagogues", new[] { "Manager_Id" });
            DropColumn("dbo.Synagogues", "Manager_Id");
        }
    }
}
