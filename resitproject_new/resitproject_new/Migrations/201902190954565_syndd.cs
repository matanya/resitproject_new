namespace resitproject_new.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class syndd : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Chairs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        I = c.Int(nullable: false),
                        J = c.Int(nullable: false),
                        SerialNumber = c.Int(nullable: false),
                        Synagogue_Id = c.Int(),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Synagogues", t => t.Synagogue_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.Synagogue_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Synagogues",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Address = c.String(),
                        Manager_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.Manager_Id)
                .Index(t => t.Manager_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Chairs", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Synagogues", "Manager_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Chairs", "Synagogue_Id", "dbo.Synagogues");
            DropIndex("dbo.Synagogues", new[] { "Manager_Id" });
            DropIndex("dbo.Chairs", new[] { "User_Id" });
            DropIndex("dbo.Chairs", new[] { "Synagogue_Id" });
            DropTable("dbo.Synagogues");
            DropTable("dbo.Chairs");
        }
    }
}
