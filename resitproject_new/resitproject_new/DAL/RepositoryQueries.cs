﻿using resitproject_new.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace resitproject_new.DAL
{
    public class RepositoryQueries
    {

        public static ApplicationDbContext db = new ApplicationDbContext();

        public static Chair GetChairBySerialNumberOfSynagogueId(int serialNumber, int synagogueId)
        {
            return db.Chairs.FirstOrDefault(c => c.SerialNumber == serialNumber && c.Synagogue.Id == synagogueId);
        }

        public static void UpdateChairOfSynagogue(int synagogueId, List<Chair> chairs)
        {
            var chairsToRemove = db.Chairs.Where(c => c.Synagogue.Id == synagogueId);
            db.Chairs.RemoveRange(chairsToRemove);
            var s = db.Synagogues.Find(synagogueId);
            chairs.ForEach(c => c.Synagogue = s);
            db.Chairs.AddRange(chairs);
            db.SaveChanges();
        }

        public static void UpdateUserChair(Chair chair, string userId)
        {
            chair.User = db.Users.Find(userId);
            db.SaveChanges();
        }

        public static Synagogue GetUserSynagogue(string userId)
        {
            Chair userChair = db.Chairs.Include(c => c.User).Include(c => c.Synagogue).FirstOrDefault(chair => chair.User.Id == userId);
            return userChair.Synagogue;
        }

        public static IEnumerable<Chair> GetUserChairsInSynagogue(string userId, int synagogueId)
        {
            IEnumerable<Chair> allChairs = GetAllChairsInSynagogue(synagogueId);
            IEnumerable<Chair> userChairs = allChairs.Where(chair => chair.User != null && chair.User.Id == userId);
            return userChairs;
        }

        public static IEnumerable<Chair> GetAllChairsInSynagogue(int synagogueId)
        {
            Synagogue synagogue = db.Synagogues.Include("Chairs.User").FirstOrDefault(s => s.Id == synagogueId);
            if (synagogue != null) {
                return synagogue.Chairs.Select(c => new Chair()
                {
                    Id = c.Id,
                    I = c.I,
                    J = c.J,
                    Synagogue = null,
                    User = c.User,
                    SerialNumber = c.SerialNumber
                }).ToList();
            }
            return null;
        }


        public static Synagogue GetManagerSynagogue(string userId)
        {
            return db.Synagogues.Include(s => s.Manager).FirstOrDefault(s => s.Manager.Id == userId);
        }

        public static bool isUserManager(string userId)
        {
            return db.Synagogues.Include(s => s.Manager).Any(s => s.Manager.Id == userId);
        }

        public static bool isUserManagerOfSynagogue(string userId, int synagogueId)
        {
            Synagogue synagogue = db.Synagogues.Include(s => s.Manager).FirstOrDefault(s => s.Id == synagogueId);
            return (synagogue != null && userId == synagogue.Manager.Id);
        }

        public static bool isAbsenceExistForChairs(IQueryable<Chair> chairs, DateTime date) {
            return db.Absences.Include(a => a.Chairs).Any(a =>
                    a.Date == date && a.Chairs.Intersect(chairs).Count() != 0);
        }

        public static bool isUserPermittedToReportAbsenceForChairs(string userId, IEnumerable<Chair> chairs) {
            if (chairs.Any(c => c.User == null)) {
                return false;
            }

            ICollection<Synagogue> synagogues = chairs.Select(chair => chair.Synagogue).Distinct().ToList();
            ICollection<ApplicationUser> users = chairs.Select(chair => chair.User).Distinct().ToList();
            if (synagogues.Count() != 1) {
                return false;
            }
            int synagogueId = synagogues.First().Id;
            bool isUserManagerOfChairsSynagaue = isUserManagerOfSynagogue(userId, synagogueId);

            bool isChairBelongsToUser = users.Count == 1 && users.First() != null && users.First().Id == userId;
            return isUserManagerOfChairsSynagaue || isChairBelongsToUser;
        }

        public static IQueryable<Chair> getDbChairsIncludeUserAndSynagogue(ICollection<Chair> Chairs)
        {
            var chairsIds = Chairs.Select(c => c.Id).ToList();
            return db.Chairs.Include(c => c.Synagogue).Include(c => c.User)
                .Where(dbChair => chairsIds.Any(chairId => chairId == dbChair.Id));
        }

        public static Absence getAbsenceIncludeUserAndSynagogue(int absenceId) {
            return db.Absences
                .Where(a => a.Id == absenceId)
                .Include(a => a.Chairs.Select(c => c.User))
                .Include(a => a.Chairs.Select(c => c.Synagogue))
                .FirstOrDefault();
        }

        public static IQueryable<Absence> GetAbsencesBySynagogue(int synagogueId)
        {
            return db.Absences
                .Include(a => a.Chairs.Select(c => c.User))
                .Include(a => a.Chairs.Select(c => c.Synagogue))
                .Where(a => a.Chairs.Any(c => c.Synagogue.Id == synagogueId));
        }

        public static int removeAbsence(Absence absence) {
            db.Absences.Remove(absence);
            return db.SaveChanges();
        }

        public static IEnumerable<Absence> GetAbsences()
        {
            return db.Absences.Include("Chairs.User");
        }

        public static IEnumerable<ApplicationUser> GetUsers()
        {
            return db.Chairs.Select(chair => chair.User).Distinct().ToList();;
        }
    }

}