﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using resitproject_new.BL;
using resitproject_new.DAL;
using resitproject_new.Models;

namespace resitproject_new.Controllers
{
    public class ChairsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private static ApplicationUserManager _userManager;
        private CSVReader _CSVReader;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public CSVReader cSVReader
        {
            get
            {
                return _CSVReader ?? CSVReader.Create(UserManager);
            }
            private set
            {
                _CSVReader = value;
            }
        }

        // GET: Chairs
        public ActionResult Index()
        {
            return View(db.Chairs.ToList());
        }

        [HttpGet]
        public ActionResult UploadChairs(int synagogueId)
        {

            Synagogue synagogue = db.Synagogues.Include(s => s.Manager).FirstOrDefault(s => s.Id == synagogueId);
            if (User.Identity.GetUserId() == synagogue.Manager.Id)
            {
                return View(synagogue);
            }
            else
            {
                ViewBag.Message = "אינך רשאי לבצע פעולה זו!";
                return View();
            }

        }

        [HttpPost]
        public ActionResult UploadChairs(HttpPostedFileBase file, int synagogueId)
        {

            try
            {
                Synagogue synagogue = db.Synagogues.Find(synagogueId);
                if (RepositoryQueries.isUserManagerOfSynagogue(User.Identity.GetUserId(), synagogueId) && file.ContentLength > 0)
                {
                    string _FileName = Path.GetFileName(file.FileName);
                    string _path = Path.Combine(Server.MapPath("~/UploadedFiles"), _FileName);
                    file.SaveAs(_path);

                    try
                    {

                        cSVReader.ReadChairCsv(_path, synagogue);
                        ViewBag.Message = "File Uploaded Successfully!!";
                    }
                    catch (Exception ex)
                    {
                        ViewBag.Message = "Chairs not saved";
                        Console.WriteLine(ex.Message);
                    }

                }

                return RedirectToAction("GetChairs", "Synagogues", new { synagogueId = synagogueId });
            }
            catch
            {
                ViewBag.Message = "File upload failed!!";
                return View();
            }
        }

        // GET: Chairs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Chair chair = db.Chairs.Find(id);
            if (chair == null)
            {
                return HttpNotFound();
            }
            return View(chair);
        }

        // GET: Chairs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Chairs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,I,J")] Chair chair)
        {
            if (ModelState.IsValid)
            {
                db.Chairs.Add(chair);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(chair);
        }

        // GET: Chairs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Chair chair = db.Chairs.Find(id);
            if (chair == null)
            {
                return HttpNotFound();
            }
            return View(chair);
        }

        // POST: Chairs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,I,J")] Chair chair)
        {
            if (ModelState.IsValid)
            {
                db.Entry(chair).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(chair);
        }

        // GET: Chairs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Chair chair = db.Chairs.Find(id);
            if (chair == null)
            {
                return HttpNotFound();
            }
            return View(chair);
        }

        // POST: Chairs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Chair chair = db.Chairs.Find(id);
            db.Chairs.Remove(chair);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
