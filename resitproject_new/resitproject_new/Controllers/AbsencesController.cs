﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using resitproject_new.Models;
using Microsoft.AspNet.Identity;
using resitproject_new.DAL;
using resitproject_new.BL;

namespace resitproject_new.Controllers
{
    public class AbsencesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Absences
        public ActionResult Index(string userName = "", string dateFilter = "", string sortBy = "")
        {
            string userId = User.Identity.GetUserId();
            if (!RepositoryQueries.isUserManager(userId))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            Synagogue currSynagogue = RepositoryQueries.GetManagerSynagogue(userId);
            ViewBag.allChairs = RepositoryQueries.GetAllChairsInSynagogue(currSynagogue.Id);

            IQueryable<Absence> absences = RepositoryQueries.GetAbsencesBySynagogue(currSynagogue.Id);

            List<AbsenceViewModel> absencesViewModel = absences.Select(delegate (Absence absence) { return AbsenceViewModel.FromAbsence(absence); }).ToList();

            if (userName != String.Empty) {
                absencesViewModel = absencesViewModel.Where(absence => absence.User.UserName.Contains(userName)).ToList();
            }

            if (dateFilter != String.Empty)
            {
                dateFilter = DateTime.Parse(dateFilter).ToString("MM/dd/yyyy");
                absencesViewModel = absencesViewModel.Where(absence => absence.Date == dateFilter).ToList();
            }

            if (sortBy == "date")
            {
                absencesViewModel = absencesViewModel.OrderBy(a => a.Date).ToList();
            }
            else if (sortBy == "user")
            {
                absencesViewModel = absencesViewModel.OrderBy(a => a.User.UserName).ToList();
            }

            ViewBag.userName = userName;
            ViewBag.dateFilter = dateFilter;

            return View(absencesViewModel);
        }

        // GET: Absences/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Absence absence = RepositoryQueries.getAbsenceIncludeUserAndSynagogue(id.Value);
            if (absence == null)
            {
                return HttpNotFound();
            }
            string userId = User.Identity.GetUserId();
            if (!RepositoryQueries.isUserPermittedToReportAbsenceForChairs(userId, absence.Chairs))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            ViewBag.isManager = RepositoryQueries.isUserManager(userId);

            AbsenceViewModel absenceViewModel = AbsenceViewModel.FromAbsence(absence);

            ViewBag.allChairs = RepositoryQueries.GetAllChairsInSynagogue(absenceViewModel.Synagogue.Id);
            // without synagogue to void circular
            ViewBag.selectedChairs = absence.Chairs.Select(chair =>  new Chair()
            {
                Id = chair.Id,
                I = chair.I,
                J = chair.J,
                SerialNumber = chair.SerialNumber,
                User = chair.User
            });

            return View(absenceViewModel);
        }

        // GET: Absences/Create
        public ActionResult Create()
        {
            string userId = User.Identity.GetUserId();
            prepareBeforeCreateView(userId);
            return View();
        }

        // POST: Absences/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Date,Chairs")] Absence absence)
        {
            string userId = User.Identity.GetUserId();
            string errorMessage = String.Empty;

            if (absence.Date == null || absence.Date == DateTime.MinValue)
            {
                errorMessage = "no shabat had been selected";
            }
            else if (absence.Chairs == null || absence.Chairs.Count() == 0)
            {
                errorMessage = "no chair had been selected";
            }
            else if (absence.Date < DateTime.Now)
            {
                errorMessage = "date of report had passed";
            }
            else {
                IQueryable<Chair> chairs = RepositoryQueries.getDbChairsIncludeUserAndSynagogue(absence.Chairs);
                if (!RepositoryQueries.isUserPermittedToReportAbsenceForChairs(userId, chairs))
                {
                    errorMessage = "you are not permitted to report about the selected chairs";
                }
                else if (RepositoryQueries.isAbsenceExistForChairs(chairs, absence.Date))
                {
                    errorMessage = "Duplicate report - one or more chairs had been reported before";
                }
                else
                {
                    var groupedChairs = chairs.GroupBy(chair => chair.User).ToList();

                    List<Absence> absencesPerUser = new List<Absence>() { };
                    groupedChairs.ForEach(userChairs => absencesPerUser.Add(new Absence()
                    {
                        Date = absence.Date,
                        Chairs = userChairs.ToList()
                    }));

                    IEnumerable<Absence> absences = RepositoryQueries.db.Absences.AddRange(absencesPerUser);

                    RepositoryQueries.db.SaveChanges();

                    string urlToReturn;
                    if (absences.Count() > 1)
                    {
                        urlToReturn = "/Absences/index";
                    }
                    else
                    {
                        urlToReturn = "/Absences/Details/" + absences.First().Id;
                    }
                    ContentResult result = new ContentResult();
                    result.Content = urlToReturn;
                    return result;
                }
            }

            return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, errorMessage);
        }

        private void prepareBeforeCreateView(string userId) {
            
            Synagogue currSynagogue;
            IEnumerable<Chair> chairsOptions = null;
            IEnumerable<Chair> allChairs = null;

            bool isManager = RepositoryQueries.isUserManager(userId);
            if (isManager)
            {
                currSynagogue = RepositoryQueries.GetManagerSynagogue(userId);
                allChairs = RepositoryQueries.GetAllChairsInSynagogue(currSynagogue.Id);
                chairsOptions = allChairs.Where(c => c.User != null);
            }
            else
            {
                currSynagogue = RepositoryQueries.GetUserSynagogue(userId);
                if (currSynagogue != null)
                {
                    chairsOptions = RepositoryQueries.GetUserChairsInSynagogue(userId, currSynagogue.Id);
                    allChairs = RepositoryQueries.GetAllChairsInSynagogue(currSynagogue.Id);
                }
            }

            ViewBag.allChairs = allChairs;
            ViewBag.isManager = isManager;
            ViewBag.currSynagogue = currSynagogue;
            ViewBag.chairsNumbersOptions = chairsOptions.Select(chair => chair.SerialNumber);
        }

        // GET: Absences/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Absence absence = RepositoryQueries.getAbsenceIncludeUserAndSynagogue(id.Value);
            //Absence absence = db.Absences.Include(a=>a.Chairs).FirstOrDefault(a => a.Id == id.Value);
            if (absence == null)
            {
                return HttpNotFound();
            }
            AbsenceViewModel absenceViewModel = AbsenceViewModel.FromAbsence(absence);
            string userId = User.Identity.GetUserId();
            bool isManager = RepositoryQueries.isUserManager(userId);
            if (absenceViewModel.User == null || (!isManager && absenceViewModel.User.Id != userId)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            IEnumerable<Chair> allChairs = RepositoryQueries.GetAllChairsInSynagogue(absenceViewModel.Synagogue.Id);
            IEnumerable<Chair> chairsOptions = RepositoryQueries.GetUserChairsInSynagogue(absenceViewModel.User.Id, absenceViewModel.Synagogue.Id);

            ViewBag.allChairs = allChairs;
            ViewBag.isManager = isManager;
            ViewBag.chairsNumbersOptions = chairsOptions.Select(chair => chair.SerialNumber);

            return View(absenceViewModel);
        }

        // POST: Absences/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Date,Chairs")] Absence absence)
        {
            string userId = User.Identity.GetUserId();
            string errorMessage = String.Empty;

            if (absence.Date == null || absence.Date == DateTime.MinValue)
            {
                errorMessage = "no shabat had been selected";
            }
            else if (absence.Chairs == null || absence.Chairs.Count() == 0)
            {
                errorMessage = "no chair had been selected";
            }
            else if (absence.Date < DateTime.Now)
            {
                errorMessage = "date of report had passed";
            }
            else
            {
                IQueryable<Chair> chairs = RepositoryQueries.getDbChairsIncludeUserAndSynagogue(absence.Chairs);
                if (!RepositoryQueries.isUserPermittedToReportAbsenceForChairs(userId, chairs))
                {
                    errorMessage = "you are not permitted to report about the selected chairs";
                }
                else
                {
                    Absence absenceToSave = RepositoryQueries.db.Absences.Find(absence.Id);
                    absenceToSave.Date = absence.Date;
                    absenceToSave.Chairs = chairs.ToList();
                    RepositoryQueries.db.Entry(absenceToSave).State = EntityState.Modified;
                    RepositoryQueries.db.SaveChanges();

                    string urlToReturn = "/Absences/Details/" + absence.Id;

                    ContentResult result = new ContentResult();
                    result.Content = urlToReturn;
                    return result;
                }
            }

            return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, errorMessage);
        }

        // POST: Absences/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Absence absence = RepositoryQueries.getAbsenceIncludeUserAndSynagogue(id);
            
            if (absence != null) {
                string userId = User.Identity.GetUserId();
                bool isUserPermittedToDelete = RepositoryQueries.isUserPermittedToReportAbsenceForChairs(userId, absence.Chairs);
                if (isUserPermittedToDelete) {
                    RepositoryQueries.removeAbsence(absence);

                    return new HttpStatusCodeResult(HttpStatusCode.OK);
                }

                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            return new HttpStatusCodeResult(HttpStatusCode.NotFound);
        }

        public ActionResult Prediction() {
            string userId = User.Identity.GetUserId();
            bool isManager = RepositoryQueries.isUserManager(userId);
            if (!isManager)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            Synagogue synagogue = RepositoryQueries.GetManagerSynagogue(userId);
            ViewBag.synagogueName = synagogue.Name;
            ViewBag.allChairs = RepositoryQueries.GetAllChairsInSynagogue(synagogue.Id);
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Prediction(string userName, DateTime date)
        {
            ApplicationUser user = db.Users.FirstOrDefault(u => u.UserName == userName);

            if (user == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "The user doesn't exist");
            }
            string predictionResult = "false";
            try
            {
                predictionResult = ML.Train(date, user.Id);
            }
            catch (Exception)
            {
            }

            ContentResult result = new ContentResult();
            result.Content = predictionResult;
            return result;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
