﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Net.Http;
using System.Web.Mvc;
using resitproject_new.Models;
using Microsoft.AspNet.Identity;
using System.IO;
using resitproject_new.BL;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using resitproject_new.DAL;

namespace resitproject_new.Controllers
{
    public class SynagoguesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private static readonly HttpClient client = new HttpClient();
        private static ApplicationUserManager _userManager;
        private CSVReader _CSVReader;
        private readonly double pageId = 412802882788210;
        private readonly string accessToken = "EAAIQSHeGIyQBAKMSlV4uZAUwbPXOnEzgLh4PUTh1vXWgxjFYSC1m9SGWuWyCQXwEDZBvdemLQFl441ZCNSM1nrfx1XVtkZCl3FKsX2cRn27hLFHbvNvxtqerzRpz2AZAESTiiK3jefhCGAWmRWxSCMF29IZChiFIjJtP5zzbhNZBqoLkjxxON8sXr677mcMusZBhBfpdgUdJIgZDZD";

    
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public CSVReader cSVReader
        {
            get
            {
                return _CSVReader  ?? CSVReader.Create(UserManager) ;
            }
            private set
            {
                _CSVReader = value;
            }
        }



        // GET: Synagogues
        public ActionResult Index()
        {
            return View(db.Synagogues.ToList());
        }

        // GET: Synagogues
        public ActionResult Manage()
        {

            string userId = User.Identity.GetUserId();
            if (userId == null)
            {
                return HttpNotFound();
            }
            
            Synagogue synagogue = db.Synagogues.FirstOrDefault(s => s.Manager.Id == userId);
            return View(synagogue);
        }


        // GET: Synagogues/Details/5
        public ActionResult Details(int id)
        {
            Synagogue synagogue = db.Synagogues.Include(String => String.Manager).FirstOrDefault(s => s.Id == id);
            if (synagogue == null)
            {
                return HttpNotFound();
            }
            return View(synagogue);
        }

        // GET: Synagogues/Create
        public ActionResult Create()
        {
            return View();
        }


        public ActionResult GetChairs(int synagogueId)
        {
            Synagogue synagogue = db.Synagogues.Include("Chairs.User").First(s1 => s1.Id == synagogueId);
            if (synagogue == null)
            {
                return HttpNotFound();
            }

            if (User.Identity != null)
            {
                ViewBag.isManager = RepositoryQueries.isUserManagerOfSynagogue(User.Identity.GetUserId(), synagogueId);
            }
            synagogue.Chairs = synagogue.Chairs.Select(c => new Chair() { I = c.I, J = c.J, Synagogue = null, User = c.User, SerialNumber = c.SerialNumber }).ToList();

            return View(synagogue);
        }

        // POST: Synagogues/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,Address,Lon,Lat")] Synagogue synagogue, HttpPostedFileBase video)
        {

            var user = db.Users.Find(User.Identity.GetUserId());
            synagogue.Manager = user;
            
            if (ModelState.IsValid)
            {
                if (video.ContentLength > 0)
                {
                    string _FileName = Path.GetFileName(video.FileName);
                    string _path = Path.Combine(Server.MapPath("~/video"),_FileName);
                    video.SaveAs(_path);
                    synagogue.VideoPath = Path.Combine("/video", _FileName);
                        
                }
                db.Synagogues.Add(synagogue);
                db.SaveChanges();

                string message = $"A new synagogue added, the name is: {synagogue.Name} the address is: {synagogue.Address}, Welcome to join";

                var responseString = await client.PostAsync(
                    $"https://graph.facebook.com/{pageId}/feed?message={message}&access_token={accessToken}", null);
                return RedirectToAction("Index");
            }

            return View(synagogue);
        }

        [HttpGet]
        public ActionResult UploadUsers(int synagogueId)
        {
            try
            {
                Synagogue synagogue = db.Synagogues.Include(s => s.Manager).FirstOrDefault(s => s.Id == synagogueId);
                if (User.Identity.GetUserId() == synagogue.Manager.Id)
                {
                    return View(synagogue);
                }
                else
                {
                    ViewBag.Message = "אינך רשאי לבצע פעולה זו!";
                    return View();
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpPost]
        public async Task<ActionResult> UploadUsers(HttpPostedFileBase file, int synagogueId)
        {
            if (RepositoryQueries.isUserManagerOfSynagogue(User.Identity.GetUserId(), synagogueId) && file.ContentLength > 0)
            {
                string _FileName = Path.GetFileName(file.FileName);
                string _path = Path.Combine(Server.MapPath("~/UploadedFiles"), _FileName);
                file.SaveAs(_path);
                Synagogue synagogue = db.Synagogues.Find(synagogueId);
                await cSVReader.ReadUsersCsv(_path, synagogue);
            }
            return RedirectToAction("GetChairs", new { synagogueId = synagogueId});
        }

        // GET: Synagogues/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Synagogue synagogue = db.Synagogues.Include(s => s.Manager).FirstOrDefault(s => s.Id == id);
            if (synagogue == null)
            {
                return HttpNotFound();
            }
            if (User.Identity.GetUserId() == synagogue.Manager.Id)
            {
                return View(synagogue);
            }

            return RedirectToAction("Index");
        }

        // POST: Synagogues/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Address,Lon,Lat")] Synagogue synagogue, HttpPostedFileBase video)
        {
            if (RepositoryQueries.isUserManagerOfSynagogue(User.Identity.GetUserId(), synagogue.Id) &&  ModelState.IsValid)
            {

                if (video != null && video.ContentLength > 0)
                {
                    string _FileName = Path.GetFileName(video.FileName);
                    string _path = Path.Combine(Server.MapPath("~/video"), _FileName);
                    video.SaveAs(_path);
                    synagogue.VideoPath = Path.Combine("/video", _FileName);

                }
                db.Entry(synagogue).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(synagogue);
        }

        // GET: Synagogues/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Synagogue synagogue = db.Synagogues.Include(s => s.Manager).FirstOrDefault(s => s.Id == id);
            if (synagogue == null)
            {
                return HttpNotFound();
            }
            if (User.Identity.GetUserId() == synagogue.Manager.Id)
            {

                return View(synagogue);
            }

            return RedirectToAction("Index");
        }

        // POST: Synagogues/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (RepositoryQueries.isUserManagerOfSynagogue(User.Identity.GetUserId(), id))
            {
                Synagogue synagogue = db.Synagogues.Find(id);
                db.Synagogues.Remove(synagogue);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
