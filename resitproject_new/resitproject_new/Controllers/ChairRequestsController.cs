﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using resitproject_new.Models;

namespace resitproject_new.Controllers
{
    public class ChairRequestsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ChairRequests
        public ActionResult Index()
        {
            var chairRequests = db.ChairRequests.Include(c => c.SpecificChair).Include(c => c.Synagogue);
            return View(chairRequests.ToList());
        }

        // GET: ChairRequests/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChairRequest chairRequest = db.ChairRequests.Find(id);
            if (chairRequest == null)
            {
                return HttpNotFound();
            }
            return View(chairRequest);
        }

        // GET: ChairRequests/Create
        public ActionResult Create()
        {
            ViewBag.SpecificChairId = new SelectList(db.Chairs, "Id", "Id");
            ViewBag.SynagogueId = new SelectList(db.Synagogues, "Id", "Name");
            return View();
        }

        // POST: ChairRequests/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,SynagogueId,RequestDate,isTempRequest,ForDate,NumOfChairs,SpecificChairId")] ChairRequest chairRequest)
        {
            if (ModelState.IsValid)
            {
                db.ChairRequests.Add(chairRequest);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SpecificChairId = new SelectList(db.Chairs, "Id", "Id", chairRequest.SpecificChairId);
            ViewBag.SynagogueId = new SelectList(db.Synagogues, "Id", "Name", chairRequest.SynagogueId);
            return View(chairRequest);
        }

        // GET: ChairRequests/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChairRequest chairRequest = db.ChairRequests.Find(id);
            if (chairRequest == null)
            {
                return HttpNotFound();
            }
            ViewBag.SpecificChairId = new SelectList(db.Chairs, "Id", "Id", chairRequest.SpecificChairId);
            ViewBag.SynagogueId = new SelectList(db.Synagogues, "Id", "Name", chairRequest.SynagogueId);
            return View(chairRequest);
        }

        // POST: ChairRequests/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,SynagogueId,RequestDate,isTempRequest,ForDate,NumOfChairs,SpecificChairId")] ChairRequest chairRequest)
        {
            if (ModelState.IsValid)
            {
                db.Entry(chairRequest).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SpecificChairId = new SelectList(db.Chairs, "Id", "Id", chairRequest.SpecificChairId);
            ViewBag.SynagogueId = new SelectList(db.Synagogues, "Id", "Name", chairRequest.SynagogueId);
            return View(chairRequest);
        }

        // GET: ChairRequests/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChairRequest chairRequest = db.ChairRequests.Find(id);
            if (chairRequest == null)
            {
                return HttpNotFound();
            }
            return View(chairRequest);
        }

        // POST: ChairRequests/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ChairRequest chairRequest = db.ChairRequests.Find(id);
            db.ChairRequests.Remove(chairRequest);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
