﻿using Microsoft.AspNet.Identity;
using resitproject_new.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using resitproject_new.DAL;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using System.Data.Entity;
using resitproject_new.BL;

namespace resitproject_new.Controllers
{
    public class SynagogueManagerController : Controller
    {

        private ApplicationDbContext db = new ApplicationDbContext();

        private static ApplicationUserManager _userManager;
        

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }


        // GET: SynagogueManager
        public ActionResult Index()
        {
           
            if (User.Identity.GetUserId() != null)
            {
                Synagogue synagogue = RepositoryQueries.GetManagerSynagogue(User.Identity.GetUserId());
                if (synagogue != null)
                {
                    return View(synagogue);
                }

                
            }
            return HttpNotFound();
        }

        private void prepareBeforeCreateView(string userId)
        {

            Synagogue currSynagogue;
            IEnumerable<Chair> chairsOptions = null;
            IEnumerable<Chair> allChairs = null;

            bool isManager = RepositoryQueries.isUserManager(userId);
            if (isManager)
            {
                currSynagogue = RepositoryQueries.GetManagerSynagogue(userId);
                chairsOptions = RepositoryQueries.GetAllChairsInSynagogue(currSynagogue.Id);
                allChairs = chairsOptions;
            }
            else
            {
                currSynagogue = RepositoryQueries.GetUserSynagogue(userId);
                if (currSynagogue != null)
                {
                    chairsOptions = RepositoryQueries.GetUserChairsInSynagogue(userId, currSynagogue.Id);
                    allChairs = RepositoryQueries.GetAllChairsInSynagogue(currSynagogue.Id);
                }
            }

            ViewBag.allChairs = allChairs;
            ViewBag.isManager = isManager;
            ViewBag.currSynagogue = currSynagogue;
            ViewBag.chairsNumbersOptions = chairsOptions.Select(chair => chair.SerialNumber);
        }

        public ActionResult GetAllUsers(int synagogueId)
        {
            if (User.Identity == null ||  !RepositoryQueries.isUserManagerOfSynagogue(User.Identity.GetUserId(), synagogueId)) 
            {
                return HttpNotFound();
            }

            var chairs = RepositoryQueries.GetAllChairsInSynagogue(synagogueId).Where(c => c.User != null);
            var charisByUsers = chairs.GroupBy(c => c.User).Select(u => new ChairsUserViewModel {User = u.Key, Chairs = u.Select(p => p.SerialNumber).ToList() }).ToList();
          
            ViewBag.synagogueId = synagogueId;
            return View(charisByUsers);
        }


        public ActionResult CreateUser(int synagogueId)
        {
            if (User.Identity == null || !RepositoryQueries.isUserManagerOfSynagogue(User.Identity.GetUserId(), synagogueId))
            {
                return HttpNotFound();
            }
            ViewBag.chairsList =  db.Chairs.Where(c => c.Synagogue.Id == synagogueId).Select(c => c.SerialNumber);
            ViewBag.synagogueId = synagogueId;
            prepareBeforeCreateView(User.Identity.GetUserId());
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateUser(int synagogueId,[Bind(Include = "User, Chairs")] ChairsUserViewModel model)
        {
            ApplicationUser user = UserManager.FindByEmailAsync(model.User.Email).Result;

            if (user == null)
            {
                user = new ApplicationUser { UserName = model.User.Email, Email = model.User.Email, PhoneNumber = model.User.PhoneNumber };
                await UserManager.CreateAsync(user, "Aa123456!");

            }
            foreach (var item in model.Chairs)
            {
                var chair = RepositoryQueries.GetChairBySerialNumberOfSynagogueId(item, synagogueId);
                RepositoryQueries.UpdateUserChair(chair, user.Id);
            }
           
            return RedirectToAction("GetAllUsers", new { synagogueId = synagogueId });
            
        }


        public ActionResult EditUser(string userId, int synagogueId)
        {
            if (User.Identity == null || !RepositoryQueries.isUserManagerOfSynagogue(User.Identity.GetUserId(), synagogueId))
            {
                return HttpNotFound();
            }
            var user = _userManager.FindById(userId);
            return View();
        }

        public ActionResult DeleteUser(string userId, int synagogueId)
        {
            if (User.Identity == null || !RepositoryQueries.isUserManagerOfSynagogue(User.Identity.GetUserId(), synagogueId))
            {
                return HttpNotFound();
            }
            var user = UserManager.FindById(userId);
            var chairs = RepositoryQueries.GetUserChairsInSynagogue(userId, synagogueId);
            ViewBag.synagogueId = synagogueId;
            return View(new ChairsUserViewModel() { User = user, Chairs = chairs.Select(c => c.SerialNumber).ToList() });
        }

        [HttpPost, ActionName("DeleteUser")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteUserConfirmed(string userId, int synagogueId)
        {
            var chiars =  RepositoryQueries.GetUserChairsInSynagogue(userId, synagogueId);
            var Synagogue = RepositoryQueries.GetManagerSynagogue(User.Identity.GetUserId());
            foreach (var chiar in chiars)
            {
                chiar.Synagogue = Synagogue;
                RepositoryQueries.UpdateUserChair(chiar, null);
            }
            
            return RedirectToAction("Index");
        }

        public ActionResult Statistics(int synagogueId)
        {
            if (User.Identity == null || !RepositoryQueries.isUserManagerOfSynagogue(User.Identity.GetUserId(), synagogueId))
            {
                return HttpNotFound();
            }
            var statisticsSynagogue = new StatisticsSynagogue(synagogueId);
            return View(new List<StatisticsViewModel>() { statisticsSynagogue.GetStatisticsByUsers(), statisticsSynagogue.GetStatisticsByDates() });
        }
        
    }

}